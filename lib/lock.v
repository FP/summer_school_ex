From iris.program_logic Require Import weakestpre.
From iris.base_logic.lib Require Import invariants.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import proofmode notation.
From lrust.lang.lib Require Export lock.
Set Default Proof Using "Type".

Definition lockN : namespace := nroot .@ "lock".

Definition mklock_unlocked : val :=
  λ: [],
    let: "l" := Alloc #1 in
    mklock_unlocked ["l"];;
    "l".

Definition mklock_locked : val :=
  λ: [],
    let: "l" := Alloc #1 in
    mklock_locked ["l"];;
    "l".

Section lock.
  Context `{!lrustG Σ}.

  Definition is_lock (l : loc) (R : iProp Σ) : iProp Σ :=
    inv lockN (lock_proto l R).

  Lemma mklock_unlocked_spec (R : iProp Σ) :
    {{{ R }}} mklock_unlocked [] {{{ l, RET #l; is_lock l R }}}.
  Proof.
    iIntros (Φ) "HR HΦ". wp_lam. wp_alloc l as "Hl" "H†". iClear "H†".
    rewrite heap_mapsto_vec_singleton. wp_let.
    wp_apply (mklock_unlocked_spec R with "[$Hl $HR]"). iIntros "H".
    iMod (inv_alloc with "H"). wp_let. by iApply "HΦ".
  Qed.

  Lemma mklock_locked_spec (R : iProp Σ) :
    {{{ True }}} mklock_locked [] {{{ l, RET #l; is_lock l R }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam. wp_alloc l as "Hl" "H†". iClear "H†".
    rewrite heap_mapsto_vec_singleton. wp_let.
    wp_apply (mklock_locked_spec R with "Hl"). iIntros "H".
    iMod (inv_alloc with "H"). wp_let. by iApply "HΦ".
  Qed.

  Lemma try_acquire_spec E l R :
    ↑lockN ⊆ E →
    {{{ is_lock l R }}} try_acquire [ #l ] @ E
    {{{ b, RET #b; (if b : bool then R else True) }}}.
  Proof.
    iIntros (? Φ) "#Hlock HΦ".
    iApply (try_acquire_spec _ _ R True with "[] [//]").
    - iIntros "!# _". iInv lockN as "$" "Hclose".
      iMod (fupd_intro_mask') as "Hclose'"; last iModIntro. set_solver.
      iIntros "H". iMod "Hclose'" as "_". by iMod ("Hclose" with "H").
    - iNext. iIntros (b) "[Hb _]". by iApply "HΦ".
  Qed.

  Lemma acquire_spec E l R :
    ↑lockN ⊆ E →
    {{{ is_lock l R }}} acquire [ #l ] @ E {{{ RET #☠; R }}}.
  Proof.
    iIntros (? Φ) "#Hlock HΦ". iApply (acquire_spec _ _ R True); [|done|].
    - iIntros "!# _". iInv lockN as "$" "Hclose".
      iMod (fupd_intro_mask') as "Hclose'"; last iModIntro. set_solver.
      iIntros "H". iMod "Hclose'" as "_". by iMod ("Hclose" with "H").
    - iNext. iIntros "[HR _]". by iApply "HΦ".
  Qed.

  Lemma release_spec E l R :
    ↑lockN ⊆ E →
    {{{ is_lock l R ∗ R }}}
      release [ #l ] @ E
    {{{ RET #☠; True }}}.
  Proof.
    iIntros (? Φ) "[#Hlock HR] HΦ".
    iApply (release_spec _ _ R True with "[] [$HR]").
    - iIntros "!# _". iInv lockN as "$" "Hclose".
      iMod (fupd_intro_mask') as "Hclose'"; last iModIntro. set_solver.
      iIntros "H". iMod "Hclose'" as "_". by iMod ("Hclose" with "H").
    - auto.
  Qed.
End lock.
