From iris.program_logic Require Import weakestpre.
From iris.base_logic.lib Require Import invariants.
From iris.proofmode Require Import tactics.
From lrust.lang Require Export spawn.
From lrust.lang Require Import proofmode notation.
Set Default Proof Using "Type".

Definition parN : namespace := nroot .@ "par".

Definition par : val :=
  λ: ["f1"; "f2"],
    let: "handle" := spawn [(λ: ["c"], "f1" [];; finish ["c"; #☠])] in
    "f2" [];;
     join ["handle"];;
     #☠.
Notation "e1 ||| e2" := (par [λ: [], e1; λ: [], e2])%E : expr_scope.

Section par.
Local Set Default Proof Using "Type*".
Context `{!lrustG Σ, spawnG Σ}.

(* Notice that this allows us to strip a later *after* the two Ψ have been
   brought together.  That is strictly stronger than first stripping a later
   and then merging them, as demonstrated by [tests/joining_existentials.v].
   This is why these are not Texan triples. *)
Lemma par_spec (Ψ1 Ψ2 : val → iProp Σ) (f1 f2 : val) (Φ : val → iProp Σ) :
  WP f1 [] {{ Ψ1 }} -∗ WP f2 [] {{ Ψ2 }} -∗
  ▷ (∀ v1 v2, Ψ1 v1 ∗ Ψ2 v2 -∗ ▷ Φ #☠) -∗
  WP par [f1; f2] {{ Φ }}.
Proof.
  iIntros "Hf1 Hf2 HΦ".
  rewrite /par. wp_lam. wp_bind (spawn _).
  iApply (spawn_spec parN (λ _, ∃ v, Ψ1 v)%I with "[Hf1]"). solve_to_val.
  - simpl. iIntros (c) "Hc". wp_let. wp_bind (f1 _). iApply (wp_wand with "Hf1").
    iIntros (v) "Hv". wp_seq. iApply (finish_spec _ _ _ #☠ with "[$Hc Hv]"); auto.
  - iIntros "!> * Hc". wp_let. wp_bind (f2 _). iApply (wp_wand with "Hf2").
    iIntros (v) "Hv". wp_seq. wp_apply (join_spec with "Hc"). iIntros (?) "HΨ1".
    iDestruct "HΨ1" as (?) "HΨ1". iSpecialize ("HΦ" with "[$Hv $HΨ1]"). wp_seq.
    iApply "HΦ".
Qed.

Lemma wp_par_strong (Ψ1 Ψ2 : val → iProp Σ)
    (e1 e2 : expr) `{!Closed [] e1, Closed [] e2} (Φ : val → iProp Σ) :
  WP e1 {{ Ψ1 }} -∗ WP e2 {{ Ψ2 }} -∗
  ▷ (∀ v1 v2, Ψ1 v1 ∗ Ψ2 v2 -∗ ▷ Φ #☠) -∗
  WP e1 ||| e2 {{ Φ }}.
Proof.
  iIntros "H1 H2 H".
  iApply (par_spec Ψ1 Ψ2 (LamV [] e1) (LamV [] e2) with "[H1] [H2] H").
  by wp_let. by wp_let.
Qed.

Lemma wp_par (Ψ1 Ψ2 : iProp Σ)
    (e1 e2 : expr) `{!Closed [] e1, Closed [] e2} (Φ : val → iProp Σ) :
  WP e1 {{ _, Ψ1 }} -∗ WP e2 {{ _, Ψ2 }} -∗
  (Ψ1 ∗ Ψ2 -∗ Φ #☠) -∗
  WP e1 ||| e2 {{ Φ }}.
Proof.
  iIntros "H1 H2 H".
  iApply (par_spec (λ _, Ψ1) (λ _, Ψ2) (LamV [] e1) (LamV [] e2) with "[H1] [H2] [H]").
  by wp_let. by wp_let. iModIntro. iIntros (_ _) "T". by iApply "H".
Qed.
End par.

Arguments wp_par [_ _ _] (_ _)%I (_ _)%E [_ _] (_)%I.
Arguments wp_par_strong [_ _ _] (_ _)%I (_ _)%E [_ _] (_)%I.
