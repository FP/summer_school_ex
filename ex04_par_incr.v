From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
From summer_school.lib Require Import par.
Set Default Proof Using "Type".

Section S.
  Local Set Default Proof Using "Type*".
  Context `{lrustG Σ, spawnG Σ}.

  Definition par_incr : val :=
    rec: "par_incr" ["p1"; "p2"] :=
      ("p1" <- !"p1" + #2)   |||   ("p2" <- !"p2" + #2).

  Lemma par_incr_correct :
    ∀ (p1 : loc) (x1 : Z) (p2 : loc) (x2 : Z),
      (p1 ↦ #x1 ∗ p2 ↦ #x2) -∗
      WP (par_incr [ #p1; #p2]) {{ _, p1 ↦ #(x1 + 2) ∗ p2 ↦ #(x2 + 2) }}.
  Proof.
    iIntros (p1 x1 p2 x2) "[Hp1 Hp2]".
    wp_rec.
    wp_apply (wp_par
      (p1 ↦ #(x1 + 2))  (* Ψ1 -- Resources for LHS *)
      (p2 ↦ #(x2 + 2))  (* Ψ2 -- Resources for RHS *)
      with "[Hp1] [Hp2]").
    - (* Proof of LHS *)
      wp_read. wp_op. wp_write. done.
    - (* Proof of RHS *)
      wp_read. wp_op. wp_write. done.
    - (* Proof that we can recombine *)
      (* auto. *)
      iIntros "H".
      done.
  Qed.
End S.