From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
Set Default Proof Using "Type".

Section S.
  Context `{lrustG Σ}.

  Fixpoint is_list (lst : list Z) (x : val) : iProp Σ :=
    match lst with
    | [] =>
      ⌜x = #0⌝
    | n::lst' =>
      ∃ (l : loc), ⌜x = #l⌝ ∗
                   l ↦ #n ∗
                   ∃ (x' : val), (l +ₗ 1) ↦ x' ∗
                                 is_list lst' x'
    end%I.

(*
  Eval simpl in (is_list []).
  Eval simpl in (is_list [42]).
  Eval simpl in (is_list [42; 18]).
*)

  Definition rev_app : val :=
    rec: "rev_app" ["l1"; "l2"] :=
      if: "l1" = #0 then "l2"
      else let: "next" := !("l1" +ₗ #1) in
           ("l1" +ₗ #1) <- "l2";;
           "rev_app" ["next"; "l1"].

  Lemma rev_app_correct :
    ∀ l1 v1 l2 v2,
      is_list l1 v1 ∗ is_list l2 v2 -∗
      WP (rev_app [v1; v2]) {{ r, is_list (rev l1 ++ l2) r }}.
  Proof.
    induction l1 as [|x l1 IH].
    - iIntros (v1 l2 v2) "(Hv1 & Hv2)".
      simpl.
      iDestruct "Hv1" as %->.
      wp_rec.
      wp_op.
      wp_if.
      iAssumption.
    - iIntros (v1 l2 v2) "(Hv1 & Hv2)".
      simpl.
      iDestruct "Hv1" as (lv1) "(Hv1 & Hx & H)".
      iDestruct "H" as (next) "(Hlv1next & Hnext)".
      iDestruct "Hv1" as %->.
      wp_rec. wp_op. wp_if. wp_op. wp_read. wp_let. wp_op. wp_write.
      rewrite -app_assoc /=.
      iApply (IH next (x :: l2) (#lv1)).
      iSplitL "Hnext".
      + done.
      + simpl.
        iExists lv1.
        iSplitL ""; [done|].
        iSplitL "Hx"; [done|].
        iExists v2.
        iSplitL "Hlv1next"; [done|done].
  Qed.

  Definition reverse : val :=
    rec: "reverse" ["l"] := rev_app ["l"; #0].

  Lemma reverse_correct :
    ∀ l v, is_list l v -∗ WP reverse [v] {{ r, is_list (rev l) r }}.
  Proof.
    iIntros (l v) "Hlv". wp_rec.
    iApply (wp_wand with "[Hlv]").
    - iApply (rev_app_correct l v [] #0).
      iSplitL "Hlv".
      + done.
      + simpl. done.
    - simpl.
      iIntros (r) "Hr".
      rewrite app_nil_r.
      done.
  Qed.

End S.
