From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
Set Default Proof Using "Type".

Section S.
  Context `{lrustG Σ}.

  Definition incr2 : val :=
    rec: "incr2" ["p1"; "p2"] :=
      "p1" <- !"p1" + #2;;
      "p2" <- !"p2" + #2.

  Lemma incr2_correct :
    ∀ (p1 : loc) (x1 : Z) (p2 : loc) (x2 : Z),
      (p1 ↦ #x1 ∗ p2 ↦ #x2) -∗
      WP (incr2 [ #p1; #p2]) {{ _, p1 ↦ #(x1 + 2) ∗ p2 ↦ #(x2 + 2) }}.
  Proof.
    iIntros (p1 x1 p2 x2) "Hp".
    iDestruct "Hp" as "[Hp1 Hp2]".
    wp_rec.
    wp_read.
    wp_op.
    wp_write.
    wp_read.
    wp_op.
    wp_write.
    iSplitL "Hp1"; done.
  Qed.

End S.
