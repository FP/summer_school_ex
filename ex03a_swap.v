From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
Set Default Proof Using "Type".

Section S.
  Context `{lrustG Σ}.

  Definition swap : val :=
    rec: "swap" ["p1"; "p2"] :=
      let: "tmp" := !"p1" in
        "p1" <- !"p2";;
        "p2" <- "tmp".

  Lemma swap_correct :
    ∀ (p1 : loc) (x1 : Z) (p2 : loc) (x2 : Z),
      p1 ↦ #x1 ∗ p2 ↦ #x2 -∗
         WP (swap [ #p1; #p2]) {{ _, p1 ↦ #x2 ∗ p2 ↦ #x1 }}.
  Proof.
    iIntros (p1 x1 p2 x2) "[Hp1 Hp2]".
    wp_rec.
    wp_read.
    wp_let.
    wp_read.
    wp_write.
    wp_write.
    iSplitL "Hp1"; [done|done].
  Qed.

End S.
