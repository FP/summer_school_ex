From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
From summer_school.lib Require Import par lock.
Set Default Proof Using "Type".

Section S.
  Local Set Default Proof Using "Type*".
  Context `{lrustG Σ, spawnG Σ}.

  Parameter resource : val → iProp Σ.
  Parameter produce_resource : val.
  Parameter produce_resource_spec :
    {{{ True }}} produce_resource [] {{{ v, RET v; resource v }}}.
  Parameter consume_resource : val.
  Parameter consume_resource_spec :
    ∀ v, {{{ resource v }}} consume_resource [v] {{{ RET #☠; True }}}.

  Definition produce_loop : val :=
    rec: "produce_loop" ["p"; "free"; "busy"] :=
      acquire ["free"];;
      let: "x" := produce_resource [] in
      "p" <- "x";;
      release ["busy"];;
      "produce_loop" ["p"; "free"; "busy"].

  Definition consume_loop : val :=
    rec: "consume_loop" ["p"; "free"; "busy"] :=
      acquire ["busy"];;
      let: "x" := !"p" in
      consume_resource ["x"];;
      release ["free"];;
      "consume_loop" ["p"; "free"; "busy"].

  Definition program : val :=
    rec: "program" ["p"] :=
      let: "free" := mklock_unlocked [] in
      let: "busy" := mklock_locked [] in
      (produce_loop ["p"; "free"; "busy"])
        ||| (consume_loop ["p"; "free"; "busy"]).










  
  Definition locks p free busy : iProp Σ :=
    (is_lock busy (∃ v, p ↦ v ∗ resource v) ∗
     is_lock free (∃ v, p ↦ v))%I.

  Lemma produce_loop_correct :
    ∀ p free busy,
      locks p free busy -∗ WP produce_loop [ #p; #free; #busy] {{ _, False }}.
  Proof.
    iIntros (p free busy) "[#Hbusy #Hfree]".
    iLöb as "IH".
    wp_rec.
    wp_apply acquire_spec; try done.
    iIntros "Hp".
    iDestruct "Hp" as (v) "Hp".
    wp_seq.
    wp_apply produce_resource_spec; first done.
    iIntros (v') "Hresource".
    wp_let.
    wp_write.
    wp_apply (release_spec with "[Hp Hresource] []"); try done. 
    - iSplitR. done. iExists _. iSplitL "Hp". done. done.
    - iIntros "!> _". wp_seq. done.
  Qed.

  Lemma consume_loop_correct :
    ∀ p free busy,
      locks p free busy -∗ WP consume_loop [ #p; #free; #busy] {{ _, False }}.
  Proof.
    iIntros (p free busy) "[#Hbusy #Hfree]".
    iLöb as "IH".
    wp_rec.
    wp_apply acquire_spec; try done.
    iIntros "Hp".
    iDestruct "Hp" as (v) "(Hp & Hresource)".
    wp_seq.
    wp_read.
    wp_let.
    wp_apply (consume_resource_spec with "Hresource").
    iIntros "_"; wp_seq.
    wp_apply (release_spec with "[Hp] []"); try done.
    - iSplitR. done. iExists _. done. 
    - iIntros "!> _". wp_seq. done.
  Qed.

  Lemma program_correct :
    ∀ p v,
      p ↦ v -∗
      WP (program [ #p]) {{ _, False }}.
  Proof.
    iIntros (p v) "Hp".
    wp_rec.
    wp_apply (mklock_unlocked_spec (∃ v, p ↦ v)%I with "[Hp]").
    { iExists _. done. }
    iIntros (free) "#Hfree".
    wp_let.
    wp_apply (mklock_locked_spec (∃ v, p ↦ v ∗ resource v)%I); first done.
    iIntros (busy) "#Hbusy".
    iAssert (locks p free busy) as "#Hlocks".
    { iSplit. done. done. }
    wp_seq.
    wp_apply (wp_par False (* Ψ1 *)
                     False (* Ψ2 *)).
    - (* Proof of LHS *)
      iApply produce_loop_correct. done.
    - (* Proof of RHS *)
      iApply consume_loop_correct. done.
    - (* Proof of the end of the program *)
      iIntros "[[] _]".
  Qed.
End S.
