From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
Set Default Proof Using "Type".

Section S.
  Context `{lrustG Σ}.

  Definition incr2 : val :=
    rec: "incr2" ["x"] :=
      "x" + #2.

  Lemma incr2_correct :
    ∀ (x : Z), (WP (incr2 [ #x]) {{ r, ⌜r = #(x+2)⌝ }})%I.
  Proof.
    iIntros (x).
    wp_rec.
    wp_op.
    done.
  Qed.
End S.
