From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
From summer_school.lib Require Import par lock.
Set Default Proof Using "Type".

Section S.
  Local Set Default Proof Using "Type*".
  Context `{lrustG Σ, spawnG Σ}.

  Definition par_incr : val :=
    rec: "par_incr" ["p"] :=
      let: "l" := mklock_unlocked [] in

      ((acquire ["l"] ;; "p" <- !"p" + #2 ;; release ["l"])
        |||
       (acquire ["l"] ;; "p" <- !"p" + #2 ;; release ["l"]));;

      acquire ["l"];; !"p".


  Lemma one_thread_correct :
    ∀ (p : loc) (l : loc),
      is_lock l (∃ x : Z, p ↦ #(2 * x)) -∗
        WP (acquire [ #l] ;; #p <- !#p + #2 ;; release [ #l])
        {{ _, True }}.
  Proof.
    iIntros (p l) "#Hl".
    wp_apply acquire_spec; try done.
    iIntros "Hinv".
    iDestruct "Hinv" as (x) "Hp".
    wp_seq. wp_read. wp_op. wp_write.
    wp_apply (release_spec with "[Hp] []"); try done.
    (* We have to reestablish the invariant. *)
    iSplitR "Hp"; try done.
    iExists (x + 1).
    rewrite Z.mul_add_distr_l. done.
    (* The LHS is finished, we prove the post-condition, which is True. *)
  Qed.
    

  Lemma par_incr_correct :
    ∀ (p : loc),
      p ↦ #0 -∗
      WP (par_incr [ #p]) {{ r, ∃ x, ⌜r = #(2 * x)⌝ }}.
  Proof.
    iIntros (p) "Hp".
    wp_rec.
    wp_apply (mklock_unlocked_spec (∃ x, p ↦ #(2 * x))%I with "[Hp]").
    - (* We establish the invariant for the first time. *)
      iExists 0.
      done.
    - (* We prove the rest of the program *)
      iIntros (l) "#Hl".
      wp_let.
      wp_apply (wp_par True  (* Ψ1 *)
                       True  (* Ψ2 *)).
      + (* Proof of LHS *)
        wp_apply one_thread_correct. done.
      + (* Proof of the RHS, exactly the same. *)
        wp_apply one_thread_correct. done.
      + (* Proof of the end of the program *)
        iIntros "_".
        wp_seq.
        wp_apply acquire_spec; try done.
        iIntros "Hinv".
        iDestruct "Hinv" as (x) "Hp".
        wp_seq. wp_read.
        iExists x.
        done.
  Qed.
End S.
