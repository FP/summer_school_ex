From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import tactics.
From lrust.lang Require Import lang proofmode notation.
Set Default Proof Using "Type".

Section S.
  Context `{lrustG Σ}.

  Definition incr2 : val :=
    rec: "incr2" ["p"] :=
      "p" <- !"p" + #2.

  Lemma incr2_correct :
    ∀ (p : loc) (x : Z),
      p ↦ #x -∗
      WP (incr2 [ #p]) {{ _, p ↦ #(x + 2) }}.
  Proof.
    iIntros (p x) "Hp".
    wp_rec.
    wp_read.
    wp_op.
    wp_write.
    done.
  Qed.
End S.
